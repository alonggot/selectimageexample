package com.m.dev.selectimageexample

import android.app.AlertDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.util.Base64
import android.util.Log
import android.view.View
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.activity_select_image.*
import pl.aprilapps.easyphotopicker.DefaultCallback
import pl.aprilapps.easyphotopicker.EasyImage
import java.io.ByteArrayOutputStream
import java.io.File

class SelectImageActivity : AppCompatActivity(),
    View.OnClickListener {

    private val SELECT_CAMERA_MENU: String = "Camara"
    private val SELECT_PHOTO_MENU: String = "photo"

    private var profileImageFiles: File? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_select_image)

        setListener()
    }

    companion object {
        const val REQUEST_MULTIPLE_PERMISSIONS = 100
    }

    private fun setListener() {
        btnSelectImage.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            btnSelectImage.id -> showDialogSelectImage()
        }
    }

    private fun showDialogSelectImage() {
        val options = arrayOf<CharSequence>(SELECT_CAMERA_MENU, SELECT_PHOTO_MENU)
        val builder = AlertDialog.Builder(this)
        builder.setItems(options) { _, item ->
            if (options[item] == SELECT_CAMERA_MENU) {
                takePhoto()
            } else if (options[item] == SELECT_PHOTO_MENU) {
                selectImageFromGallery()
            }
        }
        builder.show()
    }

    private fun selectImageFromGallery() {
        EasyImage.openGallery(this, 0)
    }

    private fun takePhoto() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkAndRequestPermissions()) {
                EasyImage.openCamera(this, 0)
            }
        } else {
            EasyImage.openCamera(this, 0)
        }
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun checkAndRequestPermissions(): Boolean {
        val cameraPermission = ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA)
        val writePermission =
            ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)

        val listPermissionsNeeded = ArrayList<String>()

        if (cameraPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.CAMERA)
        }

        if (writePermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
        }

        if (!listPermissionsNeeded.isEmpty()) {
            requestPermissions(listPermissionsNeeded.toTypedArray(), REQUEST_MULTIPLE_PERMISSIONS)
            return false
        }
        return true
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        Log.e("onRequestPermissions", "Permission callback called-------")
        when (requestCode) {
            REQUEST_MULTIPLE_PERMISSIONS -> {

                val perms = HashMap<String, Int>()
                // Initialize the map with both permissions
                perms[android.Manifest.permission.CAMERA] = PackageManager.PERMISSION_GRANTED
                perms[android.Manifest.permission.WRITE_EXTERNAL_STORAGE] = PackageManager.PERMISSION_GRANTED

                // Fill with actual results from user
                if (grantResults.isNotEmpty()) {
                    for (i in permissions.indices)
                        perms[permissions[i]] = grantResults[i]
                    // Check for both permissions
                    if (perms[android.Manifest.permission.CAMERA] == PackageManager.PERMISSION_GRANTED
                        && perms[android.Manifest.permission.WRITE_EXTERNAL_STORAGE] == PackageManager.PERMISSION_GRANTED
                    ) {
                        // process the normal flow
                        takePhoto()
                    }
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        EasyImage.handleActivityResult(requestCode, resultCode, data, this, object : DefaultCallback() {
            override fun onImagePicked(imageFile: File?, source: EasyImage.ImageSource?, type: Int) {
                profileImageFiles = imageFile
                initImage()
            }

            override fun onImagePickerError(e: Exception?, source: EasyImage.ImageSource?, type: Int) {
                //Some error handling
                e!!.printStackTrace()
            }

            override fun onCanceled(source: EasyImage.ImageSource?, type: Int) {
                //Cancel handling, you might wanna remove taken photo if it was canceled
                if (source == EasyImage.ImageSource.CAMERA) {
                    val photoFile = EasyImage.lastlyTakenButCanceledPhoto(this@SelectImageActivity)
                    photoFile?.delete()
                }
            }
        })
    }

    private fun initImage() {
        Glide.with(this)
            .load(profileImageFiles)
            .apply(
                RequestOptions()
                    .dontAnimate()
                    .skipMemoryCache(true)
            ).into(imvPreview)
    }

    fun encoderImageToBase64(filePath: String): String {
        val baos = ByteArrayOutputStream()
        val bitmap = BitmapFactory.decodeFile(filePath)
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)
        val imageBytes = baos.toByteArray()
        return Base64.encodeToString(imageBytes, Base64.DEFAULT)
    }
}
